package Controlador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehiculos.R;

import java.util.List;

import Modelo.Auto;

public class AdapterA extends RecyclerView.Adapter<AdapterA.ViewHolderAutos> {

    List<Auto> lista;

    public AdapterA(List<Auto> autos){
        this.lista = autos;
    }

    @NonNull
    @Override
    public ViewHolderAutos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, null);

        return new ViewHolderAutos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderAutos holder, int position) {

        holder.n.setText(lista.get(position).getNombre());
        holder.m.setText(lista.get(position).getMarcamodel());
        holder.p.setText(lista.get(position).getPrecio());

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public static class ViewHolderAutos extends RecyclerView.ViewHolder{

        TextView n, m, p;

        public ViewHolderAutos(@NonNull View itemView) {
            super(itemView);
            n = itemView.findViewById(R.id.vehiculo);
            m = itemView.findViewById(R.id.mm);
            p = itemView.findViewById(R.id.p);
        }
    }

}
