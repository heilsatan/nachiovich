package com.example.vehiculos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Controlador.AdapterA;
import Modelo.Auto;

public class MainActivity extends AppCompatActivity {

    List<Auto> autos;
    RecyclerView recyclerView;
    AdapterA adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recicler);
        autos = new ArrayList<>();
        Auto a1 = new Auto("Mercedes Benz", "AMG-GT", "10000");
        Auto a2 = new Auto("Mitsubishi", "Lancer", "8000");
        Auto a3 = new Auto("Mitsubishi", "Eclipse Cross", "6000");
        Auto a4 = new Auto("Mazda", "CX-5", "5000");
        Auto a5 = new Auto("Mazda", "CX-9", "4000");
        Auto a6 = new Auto("Hyundai", "Helantra 2021", "3000");
        Auto a7 = new Auto("Mercedes Benz", "GLC-SUV", "8000");
        autos.add(a1);
        autos.add(a2);
        autos.add(a3);
        autos.add(a4);
        autos.add(a5);
        autos.add(a6);
        autos.add(a7);
        cargarRecy();
        Toast.makeText(this, getIntent().getStringExtra("user"), Toast.LENGTH_SHORT).show();
    }

    private void cargarRecy(){

        adapter = new AdapterA(autos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }



}
