package Vista;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vehiculos.MainActivity;
import com.example.vehiculos.R;

import org.json.JSONException;
import org.json.JSONObject;

import Controlador.Volley;
import Controlador.sync;
import Modelo.Usuario;

public class Logear extends AppCompatActivity implements View.OnClickListener {

    EditText usn, pas;
    TextView registro;
    Button ingreso;
    Usuario u;
    Volley vo = new Volley(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logear);
        usn = findViewById(R.id.username);
        pas = findViewById(R.id.txt_password);
        registro = findViewById(R.id.goRegistro);
        ingreso = findViewById(R.id.login);
        ingreso.setOnClickListener(this);
        registro.setOnClickListener(this);
        //h.deletear();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.login:

                String usr = usn.getText().toString();
                String clave = pas.getText().toString();

                if (usr.equals("") || clave.equals("")){

                    Toast.makeText(this, "No se han ingresado datos", Toast.LENGTH_SHORT).show();

                } else {
                    vo.Logear(usr, clave, new sync() {
                        @Override
                        public void response(JSONObject json) {
                            try {
                                boolean sta = json.getBoolean("success");
                                if(sta){
                                    Toast.makeText(Logear.this, json.getString("information"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(Logear.this, MainActivity.class);
                                    intent.putExtra("user", json.getJSONObject("data").getString("cedula"));
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(Logear.this, "No registrado", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }

                break;
            case R.id.goRegistro:

                Intent intento = new Intent(Logear.this, Registrarse.class);
                startActivity(intento);

                break;

        }

    }
}
