package Modelo;

public class Auto {

    String nombre;
    String marcamodel;
    String precio;

    public Auto(String nombre, String marcamodel, String precio) {
        this.nombre = nombre;
        this.marcamodel = marcamodel;
        this.precio = precio;
    }

    public Auto(){
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarcamodel() {
        return marcamodel;
    }

    public void setMarcamodel(String marcamodel) {
        this.marcamodel = marcamodel;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
