package Vista;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vehiculos.R;

import org.json.JSONException;
import org.json.JSONObject;

import Controlador.Volley;
import Controlador.sync;
import Modelo.Usuario;

public class Registrarse extends AppCompatActivity implements View.OnClickListener {

    EditText name, pass, emal, ape;
    Button registrar;
    TextView golog;
    Volley h = new Volley(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
        name = findViewById(R.id.txt_nombre);
        pass = findViewById(R.id.txt_clave);
        emal = findViewById(R.id.txt_correo);
        ape = findViewById(R.id.txt_apellido);
        registrar = findViewById(R.id.btn_registrarse);
        golog = findViewById(R.id.goLogin);
        golog.setOnClickListener(this);
        registrar.setOnClickListener(this);

    }

    private void registro(){
        String nom = name.getText().toString();
        String pas = pass.getText().toString();
        String em = emal.getText().toString();
        String ap = ape.getText().toString();
        if (nom.equals("") || pas.equals("") || em.equals("") || ap.equals("")){
            Toast.makeText(this, "Ingresa datos primero", Toast.LENGTH_SHORT).show();
        } else {
            h.Registrarse(em, nom, ap, pas, new sync() {
                @Override
                public void response(JSONObject json) {
                    try {
                        if (json.getBoolean("success")){

                            Toast.makeText(Registrarse.this, json.getString("information"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Registrarse.this, Logear.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(Registrarse.this, "Correo ya registrado", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()){

            case R.id.goLogin:

                intent = new Intent(Registrarse.this, Logear.class);
                startActivity(intent);

                break;
            case R.id.btn_registrarse:

                registro();

                break;

        }

    }
}
