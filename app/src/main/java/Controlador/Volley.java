package Controlador;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Volley{

    private String hostcli = "http://192.168.0.108:8666";
    private String regi = "/cliente";
    private String alqu = "/alquiler";
    private String log = "/cliente/login";

    Context context;

    public Volley(Context contexto){
        this.context = contexto;
    }

    public void Logear(String c, String p, final sync s){

        String url = hostcli.concat(log);
        JSONObject json = new JSONObject();
        //yeison = new JSONObject();
        try {
            json.put("cedula", c);
            json.put("password", p);
        } catch (JSONException e) {
            Log.e("Error json", "no se pudo insertar datos en el json");
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                s.response(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo obtener datos", Toast.LENGTH_SHORT).show();
                String er = error.getMessage();
                Log.e("mal", er);
            }
        });

        Singleton.getInstance(context).addtoRequestQueue(request);

    }

    public void Registrarse(String c, String n, String a, String p, final sync s){

        String url = hostcli.concat(regi);
        final JSONObject json = new JSONObject();

        try {
            json.put("cedula", c);
            json.put("nombre", n);
            json.put("password", p);
            json.put("apellido", a);

        } catch (JSONException e) {
            Log.e("Error json", "No se pudo enviar parametros");
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                s.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo enviar datos", Toast.LENGTH_SHORT).show();
                Log.e("mal", error.getMessage());
            }
        });
        Singleton.getInstance(context).addtoRequestQueue(request);

    }



}
